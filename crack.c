#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
typedef struct 
{
    char * password;
    char * hash;
} entry ;

int stringCmp(const void *a, const void *b) {
    entry *aa = (entry*)a;
    entry *bb = (entry*)b;
    return strcmp(aa->hash,bb->hash);
}

int hashCmp(const void *key, const void *element) {
    char *kkey = (char *) key;
    entry *ee = (entry *) element;
    return strcmp(kkey,ee -> hash);
}

// TODO
// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
entry *read_dictionary(char *filename, int *size)
{
    FILE * fp = fopen(filename, "r");
    if(!fp) {
        printf("Could not open file %s\n", filename);
        exit(1);
    }
    //reading file into memory
    struct stat info;
    if(stat(filename,&info) == -1 ) {
        printf("Can't stat the file\n");
        exit(1);
    }
    int filesize = info.st_size;
    char * contents = malloc(filesize + 1);
    fread(contents, 1, filesize, fp);
    fclose(fp);
    contents[filesize] = '\0';
    
    //counting num of items/lines
    *size=0;
    for ( int i=0; i < filesize; i++ ) {
        if ( contents[i] == '\n' )
            (*size)++;
    }
    
    //printf("%d is numlines\n", *size);
    
    //Allocate space for struct of entries
    
    entry *dictionary = malloc( (*size) * sizeof(entry) );
    
    //filling dictionary
    //mem leak somewhere in here
    dictionary[0].password = strtok(contents, "\n");
    dictionary[0].hash = md5(dictionary[0].password, strlen(dictionary[0].password) );
    int e=1;
    
    //here
    while ( (dictionary[e].password = strtok(NULL, "\n")) != NULL) {
        dictionary[e].hash = md5(dictionary[e].password, strlen(dictionary[e].password) );
        e++;
    } 
    
    return dictionary;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int dsize;
    entry *dict = read_dictionary(argv[2], &dsize);
    
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict, dsize, sizeof(entry), stringCmp);
    
    // TODO
    // Open the hash file for reading.
    FILE *hash_file = fopen(argv[1], "r");
        

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    char hashwords[HASH_LEN+1] ;
    entry *found;
    int counter=0;
    while ( fgets(hashwords, HASH_LEN +1, hash_file) != NULL ) {
        *strchr(hashwords,'\n') = '\0';
        
        
        found = bsearch(hashwords, dict, dsize, sizeof(entry), hashCmp);
        if(found) {
            printf("Found hash: %s  for password %s\n", found->hash, found->password);
        }
        else {
            //printf("Couldn't find hash: %d\n", counter);
        }
        counter++;
    }
    
    fclose(hash_file);
    
    free(dict);
}

